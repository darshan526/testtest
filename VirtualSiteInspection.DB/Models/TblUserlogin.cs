﻿using System;
using System.Collections.Generic;

namespace VirtualSiteInspection.DB.Models
{
    public partial class TblUserlogin
    {
        public Guid UserLoginId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? RegisterationCompletedStatus { get; set; }
        public bool? ProfileCompletedStatus { get; set; }
        public bool? IsApproved { get; set; }

        public virtual TblRolemaster Role { get; set; }
    }
}
