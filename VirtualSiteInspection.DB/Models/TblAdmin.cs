﻿using System;
using System.Collections.Generic;

namespace VirtualSiteInspection.DB.Models
{
    public partial class TblAdmin
    {
        public Guid UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? CreatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Guid? UpdatedBy { get; set; }
        public string Status { get; set; }
    }
}
