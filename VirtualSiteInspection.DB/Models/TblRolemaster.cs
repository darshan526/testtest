﻿using System;
using System.Collections.Generic;

namespace VirtualSiteInspection.DB.Models
{
    public partial class TblRolemaster
    {
        public TblRolemaster()
        {
            TblUserlogin = new HashSet<TblUserlogin>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<TblUserlogin> TblUserlogin { get; set; }
    }
}
