﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSiteInspection.Repository.Abstract;

namespace VirtualSiteInspection.Repository.Infrastructure
{
   public interface IUnitOfWork :IDisposable
    {
        public IUserLoginRepository userLoginRepository { get; }
        public IClientRegisterRepository clientRegisterRepository { get; }
        public IRolesRepository rolesRepository { get; }

        int SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

    }
}
