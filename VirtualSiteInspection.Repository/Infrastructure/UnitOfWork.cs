﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using VirtualSiteInspection.DB.Models;
using VirtualSiteInspection.Repository.Abstract;
using VirtualSiteInspection.Repository.Concrete;

namespace VirtualSiteInspection.Repository.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly VSI_LOCAL_DBContext vsi_local_db_context;
        IDbContextTransaction transaction;
        private bool disposed = false;

        public UnitOfWork(VSI_LOCAL_DBContext dataContext )
        {
            vsi_local_db_context = dataContext;
        }
        public IUserLoginRepository _userLoginRepository { get; set; }
        public IClientRegisterRepository _clientRegisterRepository { get; set; }
        public IRolesRepository _rolesRepository { get; set; }



        public IUserLoginRepository userLoginRepository {

            get {
                if (_userLoginRepository == null)
                {
                    _userLoginRepository = new UserLoginRepository(vsi_local_db_context);
                }
                return _userLoginRepository;
            }

        }

        public IClientRegisterRepository clientRegisterRepository
        {
            get {
                if (_clientRegisterRepository == null)
                {
                    _clientRegisterRepository = new ClientRegisterRepository(vsi_local_db_context);
                }
                return _clientRegisterRepository;
            }
        }

        public IRolesRepository rolesRepository
        {
            get {
                if (_rolesRepository == null)
                {
                    _rolesRepository = new RolesRepository(vsi_local_db_context);
                }
                return _rolesRepository;
            }
        }

        public int SaveChanges()
        {
            return vsi_local_db_context.SaveChanges();
        }

        public void BeginTransaction()
        {
            if (transaction == null)
                transaction = vsi_local_db_context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (transaction != null)
                transaction.Commit();
        }

        public void RollbackTransaction()
        {
            try
            {
                if (transaction != null)
                    transaction.Rollback();
            }
            catch (Exception)
            {
                //not required currently
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                vsi_local_db_context.Dispose();
                if (transaction != null)
                    transaction.Dispose();
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
