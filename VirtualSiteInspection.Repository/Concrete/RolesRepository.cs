﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSiteInspection.DB.Models;
using VirtualSiteInspection.Repository.Abstract;

namespace VirtualSiteInspection.Repository.Concrete
{
    public class RolesRepository :IRolesRepository
    {
        readonly VSI_LOCAL_DBContext vsi_local_db_context;

        public RolesRepository(VSI_LOCAL_DBContext dataContext)
        {
            vsi_local_db_context = dataContext;
        }
    }
}
