﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualSiteInspection.DB.Models;
using VirtualSiteInspection.Repository.Abstract;

namespace VirtualSiteInspection.Repository.Concrete
{
   public class ClientRegisterRepository : IClientRegisterRepository
    {
        readonly VSI_LOCAL_DBContext vsi_local_db_context;

        public ClientRegisterRepository(VSI_LOCAL_DBContext dataContext)
        {
            vsi_local_db_context = dataContext;
        }
    }
}
